<?php

/* Helper function to autoload classes */
function __autoload($class_name) {
	$filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'aec-server-side' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
	require_once($filename);
}

/* Data layer */
$data_layer = array(
	'page_name' 	=> 'PM - Home page',
	'url' 			=> (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
	'referrer'		=> isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null,
	'user_agent' 	=> $_SERVER['HTTP_USER_AGENT'],
	'ip_client' 	=> $_SERVER['REMOTE_ADDR'],
	'host' 			=> $_SERVER['HTTP_HOST'],
	'at_property'	=> '3eecbf19-98b1-0670-248e-22c9c9f2af71',
	'lab_user'		=> 'PS2-80'
);

/* Configuration */
$config = new Adobe\Config(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ADBServerConfig.json');

/* Initialise ECID service from the AMCV cookie */
$ecid_available = FALSE;
$cookie_name = "AMCV_" . $config->getMarketingCloudConfig(Adobe\Config::MARKETINGCLOUD_ORG);
$ecid = new Adobe\ECID($config,$data_layer['user_agent'],$data_layer['ip_client']);
if (isset($_COOKIE[$cookie_name])) {
	$ecid->loadFromAMCVCookie($_COOKIE[$cookie_name]);
	$ecid_available = TRUE;
}

/* Initialise Adobe Target */
$target = $ecid_available ? new Adobe\Target($config,$ecid,$data_layer['user_agent'],$data_layer['ip_client'],$data_layer['host']) : null;

/* Initialise Adobe Analytics */ 
$analytics = $ecid_available ? new Adobe\Analytics($config,$ecid,$data_layer['user_agent'],$data_layer['ip_client']) : null;

?><!DOCTYPE HTML>
<html>

<head>
  <title><?php echo $data_layer['page_name'] ?></title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
  <script type="text/javascript" src="script/VisitorAPI.js"></script>
  <script type="text/javascript">
    var visitor = Visitor.getInstance("<?php echo $config->getMarketingCloudConfig(Adobe\Config::MARKETINGCLOUD_ORG) ?>");
    visitor.getMarketingCloudVisitorID();
  </script>
<?php if (!$target) : ?>
  <!-- No ECID present; switching to client-side Target implementation -->
  <script type="text/javascript" src="script/at.js"></script>
  <script type="text/javascript">
    function targetPageParamsAll() {
      return {
        "at_property"	: "<?php echo $data_layer['at_property'] ?>",
		"lab_user"		: "<?php echo $data_layer['lab_user'] ?>"
      };
    }
	/* Pre-hidding snippet to avoid flicker; only hiding #banner */
	;(function(win,doc,style,timeout){var STYLE_ID='at-mbox-prehide';function getParent(){return doc.getElementsByTagName('head')[0];}
    function addStyle(parent,id,def){if(!parent){return;}
    var style=doc.createElement('style');style.id=id;style.innerHTML=def;parent.appendChild(style);}
    function removeStyle(parent,id){if(!parent){return;}
    var style=doc.getElementById(id);if(!style){return;}
    parent.removeChild(style);}
    addStyle(getParent(),STYLE_ID,style);setTimeout(function(){removeStyle(getParent(),STYLE_ID);},timeout);}
    (window,document,"#banner {opacity: 0}",3000));
  </script>
<?php endif; ?>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.php">simple<span class="logo_colour">style_horizon</span></a></h1>
          <h2>Simple. Contemporary. Website Template.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="index.php">Home</a></li>
          <li><a href="examples.php">Examples</a></li>
          <li><a href="page.php">A Page</a></li>
          <li><a href="another_page.php">Another Page</a></li>
          <li><a href="contact.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      <div id="banner">
<?php
  $mbox_name = 'ps2-80-hero-banner';
  $mbox_content = null;
  if ($target) {
    // Server-side implementation, when ECID is already present
	$mbox = new Adobe\Mbox($mbox_name);
	$mbox->setPageURL($data_layer['url']);
	$mbox->addParameter("at_property", $data_layer['at_property']);
	$mbox->addParameter("lab_user", $data_layer['lab_user']);
	$target->request(array($mbox));
	$mbox_content = $mbox->getContent();
	echo "<!-- MBOX response: " . $target->getResponse() . " -->";
  }
  if ($mbox_content) :
?>
        <?php echo $mbox_content ?>
<?php 
  else : 
?>
        <img src="style/banner.jpg" />
<?php
  endif;
  if (!$target) :
?>
        <script type="text/javascript">
          adobe.target.getOffer({  
            "mbox": "<?php echo $mbox_name ?>",
            "success": function(offers) {
              adobe.target.applyOffer( { 
                "mbox": "<?php echo $mbox_name ?>",
                "selector": "#banner",
                "offer": offers 
              } );
			  document.getElementById("banner").style.opacity = "1";
            },  
            "error": function(status, error) {
              if (console && console.log) {
                console.log(status);
                console.log(error);
              }
			  document.getElementById("banner").style.opacity = "1";
            },
            "timeout": 5000
          });        
        </script>
<?php 
  endif; 
?>
      </div>
	  <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
            <h3>Latest News</h3>
            <h4>New Website Launched</h4>
            <h5>February 1st, 2014</h5>
            <p>2014 sees the redesign of our website. Take a look around and let us know what you think.<br /><a href="#">Read more</a></p>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
            <ul>
              <li><a href="#">link 1</a></li>
              <li><a href="#">link 2</a></li>
              <li><a href="#">link 3</a></li>
              <li><a href="#">link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>Welcome to the simplestyle_horizon template</h1>
        <p>This standards compliant, simple, fixed width website template is released as an 'open source' design (under a <a href="http://creativecommons.org/licenses/by/3.0">Creative Commons Attribution 3.0 Licence</a>), which means that you are free to download and use it for anything you want (including modifying and amending it). All I ask is that you leave the 'design from HTML5webtemplates.co.uk' link in the footer of the template, but other than that...</p>
        <p>This template is written entirely in <strong>HTML5</strong> and <strong>CSS</strong>, and can be validated using the links in the footer.</p>
        <p>You can view more free HTML5 web templates <a href="http://www.html5webtemplates.co.uk">here</a>.</p>
        <p>This template is a fully functional 5 page website, with an <a href="examples.php">examples</a> page that gives examples of all the styles available with this design.</p>
        <h2>Browser Compatibility</h2>
        <p>This template has been tested in the following browsers:</p>
        <ul>
          <li>Internet Explorer 9</li>
          <li>FireFox 25</li>
          <li>Google Chrome 31</li>
        </ul>
      </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index.php">Home</a> | <a href="examples.php">Examples</a> | <a href="page.php">A Page</a> | <a href="another_page.php">Another Page</a> | <a href="contact.php">Contact Us</a></p>
      <p>Copyright &copy; simplestyle_horizon | <a href="http://validator.w3.org/check?uri=referer">HTML5</a> | <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> | <a href="http://www.html5webtemplates.co.uk">Simple web templates by HTML5</a></p>
    </div>
  </div>
<?php
/* Analytics */
if ($analytics) :
  $analytics->setPageName($data_layer['page_name']);
  $analytics->setURL($data_layer['url']);
  $analytics->setEvar(1,$data_layer['page_name']);
  $analytics->setProp(1,$data_layer['url']);
  $analytics->setProp(2,'Server-side');
  $analytics->setEvent(1);
  if (isset($data_layer['referrer'])) {
    $analytics->setReferrer($data_layer['referrer']);
  }
  if (isset($mbox)) {
	$analytics->setTnta($mbox->getTnta());
  }
  $hit_url = $analytics->sendHit();
  echo "<!-- AA image request: $hit_url -->";
else :
?>
  <script type="text/javascript" src="script/AppMeasurement.js"></script>
  <script type="text/javascript">
    var s = s_gi("<?php echo $config->getAnalyticsConfig(Adobe\Config::ANALYTICS_RSIDS) ?>");
    s.visitor = visitor;
    s.trackingServer = "<?php echo $config->getAnalyticsConfig(Adobe\Config::ANALYTICS_SERVER) ?>";
    s.charSet = "<?php echo $config->getAnalyticsConfig(Adobe\Config::ANALYTICS_CHARSET) ?>";
    s.pageName = "<?php echo $data_layer['page_name'] ?>";
    s.eVar1 = "D=pageName";
    s.prop1 = "D=g";
	s.prop2 = "Client-side";
    s.events = "event1";
    s.t();
  </script>
<?php
endif;
?>
</body>
</html>
