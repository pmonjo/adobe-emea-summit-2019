<?php

namespace Adobe;

/**
 * Target class
 */
class Target {
	/**
	 * ECID object
	 */
	private $ecid;
	
	/**
	 * Link to configuration
	 */
	private $config;
	
	/**
	 * Last request to Target
	 */
	private $request;
	
	/**
	 * Last response from Target
	 */
	private $response;

	/**
	 * Request ID
	 */
	private $request_id;
	
	/**
	 * User agent of the browser
	 */
	private $user_agent;
	
	/**
	 * IP address of the client
	 */
	private $ip;
	
	/**
	 * Host
	 */
	private $host;
	
	/**
	 * Constructor
	 */
	public function __construct($config,$ecid,$user_agent,$ip_client,$host) {
		$this->config = $config;
		$this->ecid = $ecid;
		$this->user_agent = $user_agent;
		$this->ip = $ip_client;
		$this->host = $host;
	}

	/**
	 * Configure curl for the call to the ECID service
	 */
	private function curlSetopt($curl,$url) {
		$curlopts = array(
			CURLOPT_URL 			=> $url,
			CURLOPT_RETURNTRANSFER 	=> 1,
			CURLOPT_COOKIEFILE 		=> '',
			CURLOPT_USERAGENT 		=> $this->user_agent,
			CURLOPT_POSTFIELDS		=> $this->request,
			CURLOPT_PROTOCOLS		=> CURLPROTO_HTTP|CURLPROTO_HTTPS,
			CURLOPT_HTTPHEADER		=> array(
				'Cache-Control: no-cache',
				'Content-Type: application/json'
			)
		);
		if (isset($this->ip) && $this->ip != "127.0.0.1") {
			$curlopts[CURLOPT_HTTPHEADER][] = 'X-Forwarded-For: ' . $this->ip;
		}
		curl_setopt_array($curl, $curlopts);
	}
	
	/**
	 * Generate the URL to call Target
	 */
	private function getURL() {
		$url = 'http';
		if ($this->config->getMarketingCloudConfig(Config::MARKETINGCLOUD_SSL) === true) {
			$url .= 's';
		}
		$url .= '://';
		$url .= $this->config->getTargetConfig(Config::TARGET_CLIENTCODE);
		$url .= '.tt.omtrdc.net/rest/v2/batchmbox/';
		return $url;
	}
	
	/**
	 * Generate the JSON request body for the Adobe Target API1.0 
	 * @ref http://developers.adobetarget.com/api/#batch-input-parameters
	 */
	private function getRequestBody($mboxes) {
		$request = array();
		$request['client'] = $this->config->getTargetConfig(Config::TARGET_CLIENTCODE);
		$request['host'] = $this->host;
		$request['contentAsJson'] = FALSE;
		$request['id'] = array(
			'marketingCloudVisitorId' => $this->ecid->getECID()
		);
		$request['aamParameters'] = array(
			'blob' => $this->ecid->getBlob(), 
			'dcsLocationHint' => $this->ecid->getDCSRegion()
		);
		if ($this->ecid->getUUID()) {
			$request['aamParameters']['uuid'] = $this->ecid->getUUID();
		}
		$request['mboxes'] = array();
		for ($i = 0; $i < count($mboxes); $i++) {
			$request['mboxes'][] = $mboxes[$i]->toAssocArray($i);
		}
		$this->request = json_encode($request,JSON_PRETTY_PRINT);
	}

	/**
	 * Parse the HTTP response.
	 */
	private function parseResponse($curl,$mboxes) {
		if ($this->response === FALSE) {
			throw new \Exception("Adobe\\Target\\requestMbox: " . curl_error($curl) . ": " . curl_errno($curl));
		}
		$json = json_decode($this->response,TRUE);
		if ($json === NULL) {
			throw new \Exception("Adobe\\Target\\requestMbox: Could not decode JSON");
		}
		$this->request_id = $json['requestId'];
		// For each mbox in the response, find the corresponding mbox in the request and set the content
		foreach ($json['mboxResponses'] as $mbox_response) {
			if (isset($mbox_response['content'])) {
				foreach ($mboxes as $mbox) {
					if ($mbox->getName() == $mbox_response['mbox']) {
						$mbox->setContent($mbox_response['content']);
						if (isset($mbox_response['clientSideAnalyticsLoggingPayload']['tnta'])) {
							$mbox->setTnta($mbox_response['clientSideAnalyticsLoggingPayload']['tnta']);
						}
						break;
					}
				}
			}
		}
		$this->response = json_encode($json,JSON_PRETTY_PRINT);
	}

	
	/**
	 * Request contents for the given mboxes. Prefetch is currently not supported.
	 * @param $mboxes An array of mboxes
	 */
	public function request($mboxes) {
		// Initialise curl
		$curl = curl_init();
		// Create the URL
		$url = $this->getURL();
		// Generate the body
		$this->getRequestBody($mboxes);
		// Prepare HTTP request
		$this->curlSetopt($curl,$url);
		// Make the HTTP request
		$this->response = curl_exec($curl);
		// Parse the result
		$this->parseResponse($curl,$mboxes);
		// Free resources
		curl_close($curl);		
	}
	
	/**
	 * Get the last request to Target in JSON format
	 */
	public function getRequest() {
		return $this->request;
	}
	
	/**
	 * Get the last response to the Target API invokation, in JSON format
	 */
	public function getResponse() {
		return $this->response;
	}
	
}
