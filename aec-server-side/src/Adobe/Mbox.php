<?php

namespace Adobe;

/**
 * Mbox class. Only to be used with Target
 */
class Mbox {
	/**
	 * Mbox name
	 */
	private $name;
	
	/**
	 * Parameters
	 */
	private $parameters;
	
	/**
	 * Page URL
	 */
	private $url;
	
	/**
	 * Page referrer
	 */
	private $referrer;
	
	/**
	 * Content received from a Target request
	 */
	private $content;
	
	/**
	 * A4T parameter: tnta.
	 */
	private $tnta;
	
	/**
	 * Constructor
	 */
	public function __construct($name) {
		$this->name = $name;
		$this->parameters = array();
		$this->url = null;
		$this->referrer = null;
		$this->content = null;
		$this->tnta = null;
	}
	
	/**
	 * Add a parameter
	 */
	public function addParameter($key,$value) {
		$this->parameters[$key] = $value;
	}
	
	/**
	 * Set the page URL
	 */
	public function setPageURL($url) {
		$this->url = $url;
	}
	
	/**
	 * Set the page referrer
	 */
	public function setReferrer($referrer) {
		$this->referrer = $referrer;
	}
	
	/**
	 * Set the content as sent by Target in an API invocation
	 */
	public function setContent($content) {
		$this->content = $content;
	}
	
	/**
	 * Get the content after an invocation to the Target API. A value of 'null' means default content.
	 */
	public function getContent() {
		return $this->content;
	}
	
	/**
	 * Set the tnta parameter, if present in the response.
	 */
	public function setTnta($tnta) {
		$this->tnta = $tnta;
	}
	
	/**
	 * Get the tnta parameter.
	 */
	public function getTnta() {
		return $this->tnta;
	}

	/**
	 * Get the mbox name
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Convert the object into an associative array, using the Target API structure for mboxes (requests and prefetches)
	 * @ref http://developers.adobetarget.com/api/#batch-input-parameters
	 */
	public function toAssocArray($index) {
		$r = array();
		$r['indexId'] = $index;
		$r['mbox'] = $this->name;
		if (count($this->parameters)) {
			$r['parameters'] = $this->parameters;
		}
		if ($this->url) {
			$r['requestLocation']['pageURL'] = $this->url;
		}
		if ($this->referrer) {
			$r['requestLocation']['referrerURL'] = $this->referrer;
		}
		return $r;
	}
}
