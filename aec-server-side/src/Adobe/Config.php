<?php
namespace Adobe;

/**
 * Configuration class
 */
final class Config
{
	/**
	 * Constants with expected parameters
	 */
	const MARKETINGCLOUD_ORG = "org";
	const MARKETINGCLOUD_SSL = "ssl";
	const TARGET_CLIENTCODE = "clientCode";
	const TARGET_TIMEOUT = "timeout";
	const ANALYTICS_RSIDS = "rsids";
	const ANALYTICS_SERVER = "server";
	const ANALYTICS_CHARSET = "charset";
	const ANALYTICS_PRIVACYDEFAULT = "privacyDefault";
	const AUDIENCEMANAGER_SERVER = "server";
	const AUDIENCEMANAGER_ANALYTICSFORWARDINGENABLED = "analyticsForwardingEnabled";
	
	/**
	 * Internal constants for sections
	 */
	const SECTION_MARKETINGCLOUD = "marketingCloud";
	const SECTION_TARGET = "target";
	const SECTION_ANALYTICS = "analytics";
	const SECTION_AUDIENCEMANAGER = "audienceManager";
	
	/**
	 * The instance
	 */
	private static $instance = null;
	
	/**
	 * The configuration object, decoded from the JSON object.
	 */
	private $config;
	
	/**
	 * Private constructor, so nobody can use it
	 * @param $configFile Configuration file in JSON format, following the structure defined for the Mobile SDK
	 */
	public function __construct($configFile) {
		$this->config = json_decode(file_get_contents($configFile), true);
	}
	
	/**
	 * Get the version from the configuration
	 * @return The version of the configuration file, if present; 'null' otherwise
	 */
	public function getVersion() {
		return (array_key_exists("version",$this->config)) ? $this->config["version"] : null;
	}
	
	/**
	 * Internal function to check and, if present, retrieve a config parameter from one of the supported sections.
	 * @param $section Section name (case sensitive)
	 * @param $param Parameter name (case sensitive)
	 * @return The parameter value or 'null' if not present
	 */
	private function getConfigSectionParam($section,$param) {
		$ret = null;
		if (array_key_exists($section,$this->config) && array_key_exists($param,$this->config[$section])) {
			$ret = $this->config[$section][$param];
		}
		return $ret;
	}
	
	/**
	 * Get a Marketing Cloud parameter
	 * @param $marketingCloudParam Marketing Cloud parameter to look for
	 * @return The parameter value or 'null' if not present
	 */
	public function getMarketingCloudConfig($marketingCloudParam) {
		return $this->getConfigSectionParam(self::SECTION_MARKETINGCLOUD,$marketingCloudParam);
	}

	/**
	 * Get an Analytics parameter
	 * @param $analyticsParam Analytics parameter to look for
	 * @return The parameter value or 'null' if not present
	 */
	public function getAnalyticsConfig($analyticsParam) {
		return $this->getConfigSectionParam(self::SECTION_ANALYTICS,$analyticsParam);
	}

	/**
	 * Get a Target parameter
	 * @param $targetParam Target parameter to look for
	 * @return The parameter value or 'null' if not present
	 */
	public function getTargetConfig($targetParam) {
		return $this->getConfigSectionParam(self::SECTION_TARGET,$targetParam);
	}

	/**
	 * Get an Audience Manager parameter
	 * @param $audienceManagerParam Audience Manager parameter to look for
	 * @return The parameter value or 'null' if not present
	 */
	public function getAudienceManagerConfig($audienceManagerParam) {
		return $this->getConfigSectionParam(self::SECTION_AUDIENCEMANAGER,$audienceManagerParam);
	}
}
