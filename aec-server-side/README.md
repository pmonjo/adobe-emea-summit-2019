# AEC Server Side PHP

PHP implementation of ECID, AA and AT server side.

Notes:

* This code is NOT endorsed nor developed by Adobe.
* Use it at your own risk. 
* I will not provide any support and I still have to decide which license I want to release it under.
* It has been almost 10 years since I wrote code professionally. The code can be organised and written in a much better way, that is for sure. The point is not about the code itself, but how to use the Adobe tools server-side.
* The library is not finished nor comprehensive.
* The initial code only contains what I want to show. Expect a lot of changes in the code.
* The library does not address all the capabilities that the APIs offer.
* Feel free to fork it. It is just one click away.
