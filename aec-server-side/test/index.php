<?php
/* Helper function to autoload classes */
function __autoload($class_name) {
	$filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
	require_once($filename);
}

/* Data layer */
$data_layer = array(
	'page_name' 	=> 'PM - Home page',
	'url' 			=> (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
	'referrer'		=> isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null,
	'user_agent' 	=> $_SERVER['HTTP_USER_AGENT'],
	'ip_client' 	=> $_SERVER['REMOTE_ADDR'],
	'host' 			=> $_SERVER['HTTP_HOST']
);

/* Configuration */
$config = new Adobe\Config(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ADBServerConfig.json');

/* ECID service */
$cookie_name = 'ServerSideECID';
$ecid = new Adobe\ECID($config,$data_layer['user_agent'],$data_layer['ip_client']);
if (isset($_COOKIE[$cookie_name])) {
	$ecid->loadFromJSON($_COOKIE[$cookie_name]);
}
if ($ecid->getECID() === null) {
	$ecid->load();
}
setcookie($cookie_name,$ecid->toJSON(),time() + (86400 * 7)); // 86400 = 1 day

/* Target */
$mbox = new Adobe\Mbox('mbox-server-side-3');
$mbox->setPageURL($data_layer['url']);
$target = new Adobe\Target($config,$ecid,$data_layer['user_agent'],$data_layer['ip_client'],$data_layer['host']);
$target->request(array($mbox));
$mbox_content = $mbox->getContent();

/* Analytics */
$analytics = new Adobe\Analytics($config,$ecid,$data_layer['user_agent'],$data_layer['ip_client']);
$analytics->setPageName("Test page name");
$analytics->setURL($data_layer['url']);
$analytics->setEvar(1,"abc");
$analytics->setProp(1,$data_layer['url']);
$analytics->setEvent(1);
$analytics->setTnta($mbox->getTnta());
if ($data_layer['referrer']) {
	$analytics->setReferrer($referrer);
}

?>
<html>
<head>
	<title>Test for server-side implementation</title>
</head>
<body>
	<h1>Introduction</h1>
	<p>Test for the Summit</p>

	<h2>ECID service</h2>
	<ul>
		<li>ORG ID: <?php echo $config->getMarketingCloudConfig(Adobe\Config::MARKETINGCLOUD_ORG) ?></li>
		<li>ECID: <?php echo $ecid->getECID() ?></li>
		<li>Region: <?php echo $ecid->getDCSRegion() ?></li>
		<li>UUID: <?php echo $ecid->getUUID() ?></li>
		<li>Blob: <?php echo $ecid->getBlob() ?></li>
	</ul>
	<pre><?php echo $ecid->getDemdexResponse() ?></pre>

	<h2>Target</h2>
<?php if ($mbox_content): ?>
	<?php echo $mbox_content; ?>
<?php else: ?>
	<p>Welcome visitor of the world</p>
<?php endif; ?>
	<h3>Target request</h3>
	<textarea rows="20" cols="100" readonly><?php echo htmlentities($target->getRequest()) ?></textarea>
	<h3>Target response</h3>
	<textarea rows="20" cols="100" readonly><?php echo htmlentities($target->getResponse()) ?></textarea>
	
	<h2>Analytics</h2>
	<pre>
<?php
	$hit = $analytics->sendHit();
	$hit = str_replace ('&','&amp;<br/>',$hit);
	echo $hit;
?>
	</pre>
</body>
</html>
