<?php

/* Helper function to autoload classes */
function __autoload($class_name) {
	$filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
	require_once($filename);
}

try {
	$user_agent = "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0";
	$ip_client = "176.27.108.206";
	$host = "www.domain.com";
	$url = "http://$host/index.html";
	
	
	/* Config test */
	echo "*** CONFIG TESTS ***\n";
	$config = new Adobe\Config(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ADBServerConfig.json');
	echo "Config version: " . $config->getVersion() . "\n";
	echo "Config RSIDs: " . $config->getAnalyticsConfig(Adobe\Config::ANALYTICS_RSIDS) . "\n";
	echo "\n";

	/* ECID test */
	echo "*** ECID TESTS ***\n";
	// $ecid_json = '{"d_mid":"07453182422834336493075165345189007008","d_uuid":"07801441090184354883038074355651276721","d_blob":"RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y","dcs_region":6}';
	// $ecid_json = '';
	$ecid_amcv = '1994364360%7CMCIDTS%7C17988%7CMCMID%7C05344978092739894750826479640997451202%7CMCAAMLH-1554764352%7C7%7CMCAAMB-1554764352%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1554166752s%7CNONE%7CMCAID%7CNONE%7CMCSYNCSOP%7C411-17995%7CvVersion%7C3.4.0';
	$ecid = new Adobe\ECID($config,$user_agent,$ip_client);
	$ecid->loadFromAMCVCookie($ecid_amcv);
	// $ecid->loadFromJSON($ecid_json);
	if ($ecid->getECID() === null) {
		echo "No ECID available; requesting a new one\n";
		$ecid->load();
		echo "dpm.demdex.net reponse: " . $ecid->getDemdexResponse() . "\n";
	}
	echo "ECID: " . $ecid->getECID() . "\n";
	echo "UUID: " . $ecid->getUUID() . "\n";
	echo "DCS region: " . $ecid->getDCSRegion() . "\n";
	echo "ECID to JSON: " . $ecid->toJSON()  . "\n";
	echo "\n";

	/* Target tests */
	echo "*** TARGET TESTS ***\n";
	$mbox = new Adobe\Mbox('mbox-server-side-2');
	$mbox->setPageURL($url);
	$target = new Adobe\Target($config,$ecid,$user_agent,$ip_client,$host);
	$target->request(array($mbox));
	echo "Target request: " . $target->getRequest();
	echo "\n";
	echo "Target response: " . $target->getResponse();
	echo "\n";
	echo "Mbox content: " . $mbox->getContent();
	echo "\n";
	
	/* Analytics tests */
/*	echo "*** Analytics TESTS ***\n";
	$analytics = new Adobe\Analytics($config,$ecid,$user_agent,$ip_client);
	$analytics->setPageName("Test page name");
	$analytics->setURL($url);
	$analytics->setEvar(1,"abc");
	$analytics->setProp(1,$url);
	$analytics->setEvent(1);
	$url = $analytics->sendHit();
	echo "Analytics hit: $url\n";
*/
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
