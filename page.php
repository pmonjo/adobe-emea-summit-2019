<?php

/* Helper function to autoload classes */
function __autoload($class_name) {
	$filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'aec-server-side' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
	require_once($filename);
}

/* Data layer */
$data_layer = array(
	'page_name' 	=> 'PM - One page',
	'url' 			=> (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
	'referrer'		=> isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null,
	'user_agent' 	=> $_SERVER['HTTP_USER_AGENT'],
	'ip_client' 	=> $_SERVER['REMOTE_ADDR'],
	'host' 			=> $_SERVER['HTTP_HOST'],
	'at_property'	=> '3eecbf19-98b1-0670-248e-22c9c9f2af71',
	'lab_user'		=> 'PS2-80'
);

/* Configuration */
$config = new Adobe\Config(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ADBServerConfig.json');

/* Initialise ECID service from the AMCV cookie */
$ecid_available = FALSE;
$cookie_name = "AMCV_" . $config->getMarketingCloudConfig(Adobe\Config::MARKETINGCLOUD_ORG);
$ecid = new Adobe\ECID($config,$data_layer['user_agent'],$data_layer['ip_client']);
if (isset($_COOKIE[$cookie_name])) {
	$ecid->loadFromAMCVCookie($_COOKIE[$cookie_name]);
	$ecid_available = TRUE;
}

/* Initialise Adobe Target */
$target = $ecid_available ? new Adobe\Target($config,$ecid,$data_layer['user_agent'],$data_layer['ip_client'],$data_layer['host']) : null;

/* Initialise Adobe Analytics */ 
$analytics = $ecid_available ? new Adobe\Analytics($config,$ecid,$data_layer['user_agent'],$data_layer['ip_client']) : null;

?><!DOCTYPE HTML>
<html>

<head>
  <title><?php echo $data_layer['page_name'] ?></title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
  <script type="text/javascript" src="script/VisitorAPI.js"></script>
  <script type="text/javascript">
    var visitor = Visitor.getInstance("<?php echo $config->getMarketingCloudConfig(Adobe\Config::MARKETINGCLOUD_ORG) ?>");
    visitor.getMarketingCloudVisitorID();
  </script>
<?php if (!$target) : ?>
  <script type="text/javascript" src="script/at.js"></script>
  <script type="text/javascript">
    function targetPageParamsAll() {
      return {
        "at_property"	: "<?php echo $data_layer['at_property'] ?>",
		"lab_user"		: "<?php echo $data_layer['lab_user'] ?>"
      };
    }
  </script>
<?php endif; ?>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.php">simple<span class="logo_colour">style_horizon</span></a></h1>
          <h2>Simple. Contemporary. Website Template.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.php">Home</a></li>
          <li><a href="examples.php">Examples</a></li>
          <li class="selected"><a href="page.php">A Page</a></li>
          <li><a href="another_page.php">Another Page</a></li>
          <li><a href="contact.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
            <h3>Latest News</h3>
            <h4>New Website Launched</h4>
            <h5>February 1st, 2014</h5>
            <p>2014 sees the redesign of our website. Take a look around and let us know what you think.<br /><a href="#">Read more</a></p>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
            <ul>
              <li><a href="#">link 1</a></li>
              <li><a href="#">link 2</a></li>
              <li><a href="#">link 3</a></li>
              <li><a href="#">link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>A Page</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.</p>
      </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index.php">Home</a> | <a href="examples.php">Examples</a> | <a href="page.php">A Page</a> | <a href="another_page.php">Another Page</a> | <a href="contact.php">Contact Us</a></p>
      <p>Copyright &copy; simplestyle_horizon | <a href="http://validator.w3.org/check?uri=referer">HTML5</a> | <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> | <a href="http://www.html5webtemplates.co.uk">Simple web templates by HTML5</a></p>
    </div>
  </div>
<?php
/* Analytics */
if ($analytics) :
  $analytics->setPageName($data_layer['page_name']);
  $analytics->setURL($data_layer['url']);
  $analytics->setEvar(1,$data_layer['page_name']);
  $analytics->setProp(1,$data_layer['url']);
  $analytics->setProp(2,'Server-side');
  $analytics->setEvent(1);
  if (isset($data_layer['referrer'])) {
    $analytics->setReferrer($data_layer['referrer']);
  }
  if (isset($mbox)) {
	$analytics->setTnta($mbox->getTnta());
  }
  $hit_url = $analytics->sendHit();
  echo "<!-- AA image request: $hit_url -->";
else :
?>
  <script type="text/javascript" src="script/AppMeasurement.js"></script>
  <script type="text/javascript">
    var s = s_gi("<?php echo $config->getAnalyticsConfig(Adobe\Config::ANALYTICS_RSIDS) ?>");
    s.visitor = visitor;
    s.trackingServer = "<?php echo $config->getAnalyticsConfig(Adobe\Config::ANALYTICS_SERVER) ?>";
    s.charSet = "<?php echo $config->getAnalyticsConfig(Adobe\Config::ANALYTICS_CHARSET) ?>";
    s.pageName = "<?php echo $data_layer['page_name'] ?>";
    s.eVar1 = "D=pageName";
    s.prop1 = "D=g";
	s.prop2 = "Client-side";
    s.events = "event1";
    s.t();
  </script>
<?php
endif;
?>
</body>
</html>

